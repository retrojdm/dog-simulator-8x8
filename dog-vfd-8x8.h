/*----------+
| Constants |
+----------*/
#define VFD_BAUD_RATE 115200       // Default is 38400. See VFD Jumpers for more options.
#define PIN_VFD_SERIAL_RX       2 // Not actually used
#define PIN_VFD_SERIAL_TX       3 // This goes to the SIN pin on the VFD
#define PIN_VFD_SBUSY           4
#define PIN_VFD_RESET           6

#define VFD_DISPLAY_WIDTH 112 // dots
#define VFD_DISPLAY_HEIGHT 16 // dots
#define VFD_MEMORY_WIDTH 512  // dots
#define VFD_MEMORY_HEIGHT 16  // dots

#define PIN_BUZZER              7 // Passive buzzer
#define PIN_BUTTON_LEFT         8
#define PIN_BUTTON_RIGHT        9
#define PIN_BUTTON_JUMP        10
#define PIN_BUTTON_POOP        11

#define BUTTONS                 4
#define BUTTON_LEFT             0
#define BUTTON_RIGHT            1
#define BUTTON_JUMP             2
#define BUTTON_POOP             3

#define FRAME_RATE             25 // frames per second
#define FRAME_DELAY          1000 / FRAME_RATE
#define MAX_FRAMESKIP          25
    
#define ANIM_STAND              0
#define ANIM_SIT                1
#define ANIM_LAY                2
#define ANIM_SLEEP              3
#define ANIM_WALK               4
#define ANIM_JUMP               5
#define ANIM_FLOAT              6
#define ANIM_FALL               7
#define ANIM_POOP               8
#define ANIM_EAT                9

#define STATE_NONE              0
#define STATE_STANDING          1
#define STATE_SITTING           2
#define STATE_LAYING            3
#define STATE_SLEEPING          4
#define STATE_MOVING            5
#define STATE_POOP_ATTEMPT      6
#define STATE_POOPING           7
#define STATE_POOP_FINISHING    8
#define STATE_EATING            9

#define FALL_STRENGTH        0.25
#define MAX_FALL_SPEED       2.75
#define FLOAT_THRESHOLD       0.5

#define PLAYER_WIDTH            8 // The "player" sprite.
#define PLAYER_HEIGHT           8 // Sprite heights must be multiples of 8px
#define PLAYER_X_OFFSET         4
#define PLAYER_Y_OFFSET         7
#define PLAYER_Y_DISPLAY_OFFSET 11
#define PLAYER_WALK_SPEED       1
#define PLAYER_JUMP_STRENGTH    MAX_FALL_SPEED
#define PLAYER_COLLISION_RANGE  3 // How many pixels from the player before we collide with a treat?

#define TREAT_SPRITE           19
#define TREAT_WIDTH             3
#define TREAT_HEIGHT            8 // Sprite heights must be multiples of 8px
#define TREAT_X_OFFSET          1
#define TREAT_Y_OFFSET          7
#define MAX_TREATS              8

#define POOP_SPRITE            20
#define POOP_WIDTH              5
#define POOP_HEIGHT             8 // Sprite heights must be multiples of 8px
#define POOP_X_OFFSET           2
#define POOP_Y_OFFSET           7
#define MAX_POOPS      MAX_TREATS // Because you need to eat treats to make poops.

#define TILE_WIDTH              8 // px
#define TILE_HEIGHT             8 // px
#define LEVEL_WIDTH            36 // Tiles (the display is 18 tiles wide)
#define LEVEL_HEIGHT           16 // Tiles (the display is  2 tiles high)
#define LEVEL_X2                LEVEL_WIDTH * TILE_WIDTH - 1
#define LEVEL_Y2                LEVEL_HEIGHT * TILE_HEIGHT - 1

// Special tiles
#define TILE_FLOWER             2
#define TILES_SOLID_START       4
#define TILE_PLAYER            41
#define TILE_TREAT             42


#define FLOWER_FRAMES           2
#define FLOWER_TICKS           12



/*-----------+
| Structures |
+-----------*/

struct animType {
    byte length;
    byte frameDelay; // How many game ticks before a new animation frame?
    byte frames[6];  // We can't easily initialize jagged arrays, so I've just made them all a fixed max length.
};

struct objectType {
    float x, y;                // Where is it on the display.
    float dx, dy;              // this is the object's velocity
    byte anim;                 // Which animation are we showing? Eg: Stand, Sit, Walk, Bark, Poop
    byte tick;                 // There can be multiple game ticks to wait between frames
    byte frame;                // Which frame of the animation are we on? (not which sprite to draw)
    byte state;                // Eg: Idle, Walking, Barking, Pooping.
    byte nextState;
    byte returnToState;
    byte ticksUntilNextState;  // How long before going back to the idle state? 0 = never.
};

struct buttonType {
    byte    pin;
    boolean was; // was pressed in the last loop pass?
    boolean is;  // is pressed  (digitalRead(.pin) == LOW)?
};



/*-----------------+
| Global Variables |
+-----------------*/
// Only needed if we're not using a hardware serial. Eg: Serial or Serial1 etc.
SoftwareSerial vfdSerial(
  PIN_VFD_SERIAL_RX,
  PIN_VFD_SERIAL_TX);

// An instance of the gu7000UartHal class.
// This is a Hardware Abstraction layer for asynchronous serial (UART) mode.
// See gu7000I2cHal or write a custom HAL for other modes if your display supports them.
gu7000UartHal vfdHal(
  &vfdSerial,
  PIN_VFD_SBUSY,
  PIN_VFD_RESET);

// Error code "2" means we timed out waiting over 1 second for the SBusy pin to go LOW.
// This could occur if we didn't reset properly or if the read buffer is full.
static void errorCallback(uint8_t code)
{
    Serial.begin(9600);
    Serial.print("Error ");
    Serial.println(code);
    while (true)
        ;
}

// An instance of the gu7000 class.
gu7000 vfd(
  VFD_DISPLAY_WIDTH,
  VFD_DISPLAY_HEIGHT,
  VFD_MEMORY_WIDTH,
  VFD_MEMORY_HEIGHT,
  &vfdHal,
  errorCallback);


/*
You can generate sprites using either of the following tools:

    http://www.noritake-elec.com/codeLibraries/gu7000/gu7000_BitmapImageTool.html
    http://en.radzio.dxp.pl/bitmap_converter/
*/

/*--------+
| Sprites |
+--------*/
// Player Stand
static const byte PROGMEM sprite_plater_stand_0[] = { 0x0f,0x16,0x07,0x66,0x3f,0x5e,0x37,0x30 };  //  0
static const byte PROGMEM sprite_plater_stand_1[] = { 0x1f,0x06,0x07,0x66,0x3f,0x5e,0x37,0x30 };  //  1

// Player Sit
static const byte PROGMEM sprite_plater_sit_0[] = { 0x04,0x0b,0x03,0x67,0x3f,0x5e,0x37,0x30 };    //  2
static const byte PROGMEM sprite_plater_sit_1[] = { 0x0c,0x03,0x03,0x67,0x3f,0x5e,0x37,0x30 };    //  3

// Player Lay
static const byte PROGMEM sprite_plater_lay_0[] = { 0x07,0x0b,0x03,0x1b,0x0f,0x17,0x0d,0x0d };    //  4
static const byte PROGMEM sprite_plater_lay_1[] = { 0x0f,0x03,0x03,0x1b,0x0f,0x17,0x0d,0x0d };    //  5

// Player Sleep
static const byte PROGMEM sprite_plater_sleep_0[] = { 0x01,0x03,0x01,0x06,0x06,0x0e,0x06,0x0d };  //  6
static const byte PROGMEM sprite_plater_sleep_1[] = { 0x01,0x03,0x01,0x16,0x06,0x0e,0x06,0x0d };  //  7
static const byte PROGMEM sprite_plater_sleep_2[] = { 0x01,0x03,0x31,0x36,0x06,0x0e,0x06,0x0d };  //  8
static const byte PROGMEM sprite_plater_sleep_3[] = { 0x01,0x53,0x71,0x56,0x06,0x0e,0x06,0x0d };  //  9
static const byte PROGMEM sprite_plater_sleep_4[] = { 0x91,0xb3,0xd1,0x96,0x06,0x0e,0x06,0x0d };  // 10
static const byte PROGMEM sprite_plater_sleep_5[] = { 0x91,0x03,0x01,0x96,0x06,0x0e,0x06,0x0d };  // 11

// Player Walk
static const byte PROGMEM sprite_plater_walk_0[] = { 0x0f,0x16,0x07,0xce,0x7c,0xbe,0x6c,0x62 };   // 12
static const byte PROGMEM sprite_plater_walk_1[] = { 0x1e,0x2c,0x0e,0xcc,0x7c,0xbe,0x6c,0x62 };   // 13
static const byte PROGMEM sprite_plater_walk_2[] = { 0x7e,0x1c,0x0e,0x6c,0x3f,0x5e,0x37,0x30 };   // 14
static const byte PROGMEM sprite_plater_walk_3[] = { 0x1e,0x07,0x07,0x66,0x3f,0x5f,0x36,0x30 };   // 15

// Player Poop
static const byte PROGMEM sprite_plater_poop_0[] = { 0x3f,0x0c,0x0f,0x36,0x37,0x5e,0x3f,0x70 };   // 16

// Player Eat
static const byte PROGMEM sprite_plater_eat_0[] = { 0x0f,0x16,0x07,0x1e,0x0f,0x17,0x0d,0x0c };    // 17
static const byte PROGMEM sprite_plater_eat_1[] = { 0x1f,0x06,0x07,0x0e,0x07,0x0b,0x07,0x06 };    // 18

// Items
static const byte PROGMEM sprite_treat[] = { 0x02,0x07,0x02 };                                    // 19
static const byte PROGMEM sprite_poop[] = { 0x01,0x03,0x07,0x0b,0x01 };                           // 20

sprite sprites[] = {
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_stand_0 },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_stand_1 },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_sit_0   },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_sit_1   },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_lay_0   },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_lay_1   },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_sleep_0 },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_sleep_1 },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_sleep_2 },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_sleep_3 },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_sleep_4 },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_sleep_5 },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_walk_0  },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_walk_1  },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_walk_2  },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_walk_3  },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_poop_0  },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_eat_0   },
    { PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_X_OFFSET, PLAYER_Y_OFFSET, sprite_plater_eat_1   },
    { TREAT_WIDTH,  TREAT_HEIGHT,  TREAT_X_OFFSET,  TREAT_Y_OFFSET,  sprite_treat          },
    { POOP_WIDTH,   POOP_HEIGHT,   POOP_X_OFFSET,   POOP_Y_OFFSET,   sprite_poop           }
};

// The number of frames, frame delay, and which frames for each animation are defined here.
animType anims[10] = {
    { 2, 4, {  0,  1,  0,  0,  0,  0 } },
    { 2, 4, {  2,  3,  0,  0,  0,  0 } },
    { 2, 4, {  4,  5,  0,  0,  0,  0 } },
    { 6, 8, {  6,  7,  8,  9, 10, 11 } },
    { 4, 2, { 12, 13, 14, 15,  0,  0 } },
    { 1, 1, { 12,  0,  0,  0,  0,  0 } },    
    { 1, 1, { 13,  0,  0,  0,  0,  0 } },
    { 1, 1, { 14,  0,  0,  0,  0,  0 } },
    { 1, 1, { 16,  0,  0,  0,  0,  0 } },
    { 2, 4, { 17, 18,  0,  0,  0,  0 } }
};



/*-----------------+
| Background Tiles |
+-----------------*/
   
    
static const byte PROGMEM tile_grass[]  = { 0x01,0x05,0x03,0x01,0x0f,0x01,0x03,0x01 };
static const byte PROGMEM tile_flower[] = { 0x00,0x00,0x64,0x9f,0x92,0x60,0x00,0x00 };
static const byte PROGMEM tile_flower2[] = { 0x00,0x00,0x34,0x4f,0x4a,0x30,0x00,0x00 };
    
static const byte PROGMEM tile_block_xxxx[] = { 0x55,0xaa,0x55,0xaa,0x55,0xaa,0x55,0xaa };
static const byte PROGMEM tile_block_xxxR[] = { 0x55,0xaa,0x55,0xaa,0x55,0xaa,0x55,0xff };
static const byte PROGMEM tile_block_xxLx[] = { 0xff,0xaa,0x55,0xaa,0x55,0xaa,0x55,0xaa };
static const byte PROGMEM tile_block_xxLR[] = { 0xff,0xaa,0x55,0xaa,0x55,0xaa,0x55,0xff };
static const byte PROGMEM tile_block_xBxx[] = { 0x55,0xab,0x55,0xab,0x55,0xab,0x55,0xab };
static const byte PROGMEM tile_block_xBxR[] = { 0x55,0xab,0x55,0xab,0x55,0xab,0x55,0xfe };
static const byte PROGMEM tile_block_xBLx[] = { 0xfe,0xab,0x55,0xab,0x55,0xab,0x55,0xab };
static const byte PROGMEM tile_block_xBLR[] = { 0xfe,0xab,0x55,0xab,0x55,0xab,0x55,0xfe };
static const byte PROGMEM tile_block_Txxx[] = { 0x95,0xaa,0x95,0xaa,0x95,0xaa,0x95,0xaa };
static const byte PROGMEM tile_block_TxxR[] = { 0x95,0xaa,0x95,0xaa,0x95,0xaa,0x95,0x7f };
static const byte PROGMEM tile_block_TxLx[] = { 0x7f,0xaa,0x95,0xaa,0x95,0xaa,0x95,0xaa };
static const byte PROGMEM tile_block_TxLR[] = { 0x7f,0xaa,0x95,0xaa,0x95,0xaa,0x95,0x7f };
static const byte PROGMEM tile_block_TBxx[] = { 0x95,0xab,0x95,0xab,0x95,0xab,0x95,0xab };
static const byte PROGMEM tile_block_TBxR[] = { 0x95,0xab,0x95,0xab,0x95,0xab,0x95,0x7e };
static const byte PROGMEM tile_block_TBLx[] = { 0x7e,0xab,0x95,0xab,0x95,0xab,0x95,0xab };
static const byte PROGMEM tile_block_TBLR[] = { 0x7e,0xab,0x95,0xab,0x95,0xab,0x95,0x7e };


sprite tiles[] = {
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_grass },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_flower },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_flower2 },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_xxxx },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_xxxR },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_xxLx },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_xxLR },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_xBxx },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_xBxR },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_xBLx },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_xBLR },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_Txxx },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_TxxR },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_TxLx },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_TxLR },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_TBxx },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_TBxR },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_TBLx },
    { TILE_WIDTH, TILE_HEIGHT, 0, 0, tile_block_TBLR }
};



// The display is 9 tiles wide.
static const byte PROGMEM level[LEVEL_WIDTH * LEVEL_HEIGHT] = { 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 42, 0, 0, 0, 0, 0, 0, 2, 0, 42, 0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 7, 42, 0, 1, 0, 0, 0, 0, 2, 5, 0, 0, 0, 18, 17, 0, 0, 0, 0, 0, 18, 16, 16, 16, 16, 16, 16, 17, 0, 0, 14, 13, 0, 0, 14, 12, 4, 12, 12, 12, 17, 0, 0, 14, 12, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 5, 0, 0, 6, 4, 4, 4, 4, 9, 0, 0, 14, 4, 4, 5, 0, 0, 0, 0, 0, 0, 0, 18, 17, 0, 0, 15, 0, 0, 0, 0, 0, 0, 0, 0, 10, 9, 0, 0, 10, 8, 8, 8, 9, 0, 0, 14, 4, 4, 4, 5, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 6, 12, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 4, 4, 4, 4, 5, 0, 0, 0, 0, 18, 17, 0, 0, 0, 0, 0, 6, 4, 5, 0, 0, 42, 0, 0, 0, 0, 0, 1, 42, 0, 0, 0, 0, 0, 14, 4, 4, 4, 4, 4, 5, 0, 2, 42, 0, 0, 0, 0, 0, 0, 0, 0, 10, 8, 8, 16, 16, 16, 16, 17, 0, 0, 0, 14, 13, 0, 0, 0, 0, 14, 4, 4, 8, 8, 8, 4, 4, 16, 16, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 9, 0, 0, 18, 16, 8, 8, 9, 0, 0, 0, 6, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 5, 0, 0, 0, 0, 18, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 5, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 13, 0, 42, 0, 6, 5, 0, 0, 0, 0, 0, 0, 0, 18, 17, 0, 0, 0, 0, 0, 0, 0, 0, 14, 4, 5, 0, 0, 0, 0, 0, 0, 0, 0, 14, 4, 4, 12, 12, 12, 4, 5, 0, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0, 41, 0, 0, 1, 0, 14, 4, 4, 4, 13, 2, 0, 0, 0, 0, 0, 14, 4, 4, 4, 4, 4, 4, 4, 4, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 4, 4, 4, 4, 4, 13, 0, 0, 0, 0, 14, 4, 4, 4, 4, 4, 4, 4, 4 };

byte mapData[LEVEL_WIDTH * LEVEL_HEIGHT];



// The player ("Sunny" the pooch)
objectType player;

// The poop objects.
byte poops = 0;
objectType poop[MAX_POOPS];

// The treat objects.
byte treats        = 0;
byte treatsInBelly = 0;
objectType treat[MAX_TREATS];


// When do we need to move, and draw again? (set by a call to millis())
unsigned long nextTick = 0;

// The flowers are animated.
byte flowerFrame = 0;
byte flowerTick  = 0;

// The 4 buttons.
buttonType button[BUTTONS] = {
    { PIN_BUTTON_LEFT, false, false },
    { PIN_BUTTON_RIGHT, false, false },
    { PIN_BUTTON_JUMP, false, false },
    { PIN_BUTTON_POOP, false, false }
};
