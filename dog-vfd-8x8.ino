////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// dog-vfd-8x8.ino
//
// Andrew Wyatt
// retrojdm.com
//
// To Do:       Draw better level GFX.
//              Add Enemies
//              Add level goal
//              Add multiple levels
//              Add "Good Girl" / "Bad Girl" status at end of level (for pooping where you should).

#include <SoftwareSerial.h> // If you want to use a hardware serial instead, you don't need this.
#include <gu7000.h>
#include <hal/gu7000UartHal.h>
#include "Arduino.h"
#include "dog-vfd-8x8.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Functions

void setup()
{
    // Set the button pins as inputs, with a pullup resistor.
    // This means they're usually HIGH, and are pulled LOW when pressed.
    for (uint8_t b = 0; b < BUTTONS; b++)
    {
        pinMode(button[b].pin, INPUT_PULLUP);
    }

    // Buzzer
    pinMode(PIN_BUZZER, OUTPUT);

    // We need to begin the Serial before calling vfd.init();
    pinMode(PIN_VFD_SERIAL_RX, INPUT);
    pinMode(PIN_VFD_SERIAL_TX, OUTPUT);
    vfdSerial.begin(VFD_BAUD_RATE);

    vfd.reset();
    vfd.init();

    vfd.brightness(4); // 1..8

    // We need to set up a window in the hidden display memory.
    // We'll draw to this hidden part, and when finished, we'll "shift" it into view.
    // This will stop the flickering that occours from the .cls() when drawing directly to the visible part of the
    // display.
    vfd.writeScreenMode(1); // Allow drawing to all the display memory (not just the visible part).
    vfd.windowDefine(1, VFD_DISPLAY_WIDTH, 0, VFD_DISPLAY_WIDTH, VFD_DISPLAY_HEIGHT >> 3);
    vfd.windowSelect(1);

    // OR the pixels together, instead of overwrite. This effectively makes "0" pixels transparent.
    vfd.writeMixDisplayMode(1);

    // place the puppy and treats from the level data.
    initLevel();
}

void loop()
{
    // This is the main game loop.

    // On slower hardware (or when there's a lot to render), we skip frames.
    uint8_t loops = 0;
    while ((millis() > nextTick) && (loops < MAX_FRAMESKIP))
    {
        readButtons();
        updateGame();

        nextTick += FRAME_DELAY;
        loops++;
    }

    render();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions

void changeState(objectType *obj, uint8_t state)
{
    // Changing the state does effect the player's position or velocity.

    obj->state = state;
    obj->tick = 0;
    obj->frame = 0;

    switch (state)
    {
    case STATE_STANDING:
        obj->anim = ANIM_STAND;
        obj->nextState = STATE_SITTING;
        obj->ticksUntilNextState = 25;
        break;

    case STATE_SITTING:
        obj->anim = ANIM_SIT;
        obj->nextState = STATE_LAYING;
        obj->ticksUntilNextState = 50;
        break;

    case STATE_LAYING:
        obj->anim = ANIM_LAY;
        obj->nextState = STATE_SLEEPING;
        obj->ticksUntilNextState = 100;
        break;

    case STATE_SLEEPING:
        obj->anim = ANIM_SLEEP;
        obj->nextState = STATE_NONE;
        obj->ticksUntilNextState = 0;
        break;

    case STATE_MOVING:
        obj->anim = ANIM_WALK;
        obj->nextState = STATE_NONE;
        obj->ticksUntilNextState = 0;
        break;

    case STATE_POOP_ATTEMPT:
        obj->anim = ANIM_POOP;
        obj->nextState = STATE_POOPING;
        obj->ticksUntilNextState = 12;
        break;

    case STATE_POOPING:
        obj->anim = ANIM_POOP;
        obj->nextState = STATE_POOP_FINISHING;
        obj->ticksUntilNextState = 1; // This must be 1, otherwise you'll drop more than one poop at a time.
        break;

    case STATE_POOP_FINISHING:
        obj->anim = ANIM_POOP;
        obj->nextState = obj->returnToState;
        obj->ticksUntilNextState = 4;
        break;

    case STATE_EATING:
        obj->anim = ANIM_EAT;
        obj->nextState = obj->returnToState;
        obj->ticksUntilNextState = 12;
        break;
    }
}

void readButtons()
{
    // This function adds PRESS and RELEASE events to the queue.

    // cycle through each button...
    for (uint8_t b = 0; b < BUTTONS; b++)
    {
        // Remember the state of this button from the last pass.
        button[b].was = button[b].is;
        button[b].is = digitalRead(button[b].pin) == LOW;

        // Is the button down?
        if (button[b].is)
        {
            // Just pressed?
            if (!(button[b].was))
            {
                switch (b)
                {

                case BUTTON_LEFT:
                    changeState(&player, STATE_MOVING);
                    player.dx = -PLAYER_WALK_SPEED;
                    break;

                case BUTTON_RIGHT:
                    changeState(&player, STATE_MOVING);
                    player.dx = PLAYER_WALK_SPEED;
                    break;

                case BUTTON_JUMP:
                    if (playerIsAboveASolidTile() && !playerIsUnderASolidTile())
                    {
                        if ((player.state >= STATE_STANDING) && (player.state <= STATE_SLEEPING))
                        {
                            player.state = STATE_STANDING;
                        }

                        player.dy = -PLAYER_JUMP_STRENGTH;
                        player.tick = 0;
                        player.frame = 0;
                        player.anim = ANIM_JUMP;
                    }
                    break;

                case BUTTON_POOP:
                    if ((playerIsAboveASolidTile()) && (treatsInBelly))
                    {
                        if ((player.state >= STATE_STANDING) && (player.state <= STATE_SLEEPING))
                        {
                            player.returnToState = STATE_STANDING;
                        }
                        else
                        {
                            player.returnToState = player.state;
                        }

                        changeState(&player, STATE_POOP_ATTEMPT);
                    }
                    break;
                }
            }
        }

        // Not down?
        else
        {
            // Just released?
            if (button[b].was)
            {
                switch (b)
                {

                case BUTTON_LEFT:
                    if (button[BUTTON_RIGHT].is)
                    {
                        player.dx = PLAYER_WALK_SPEED;
                    }
                    else
                    {
                        changeState(&player, STATE_STANDING);
                    }

                    break;

                case BUTTON_RIGHT:
                    if (button[BUTTON_LEFT].is)
                    {
                        player.dx = -PLAYER_WALK_SPEED;
                    }
                    else
                    {
                        changeState(&player, STATE_STANDING);
                    }

                    break;
                }
            }
        }
    }
}

void collisionDetection()
{

    // This function assumes that a treat can't be in the same position as another.

    for (uint8_t t = 0; t < treats; t++)

        if ((abs(player.x - treat[t].x) <= PLAYER_COLLISION_RANGE) &&
            (abs(player.y - treat[t].y) <= PLAYER_COLLISION_RANGE))
        {
            treatsInBelly++;

            // move the last treat to the collided one, and shorten the list.
            treat[t] = treat[treats - 1];
            treats--;

            player.returnToState = player.state;
            changeState(&player, STATE_EATING);
            break;
        }
}

boolean isInASolidTile(int16_t x, int16_t y)
{
    if (y > LEVEL_Y2)
    {
        return true;
    }
    else if ((x >= 0) &&
             (x <= LEVEL_X2) &&
             (y >= 0))
    {
        return (mapData[(y / TILE_HEIGHT) * LEVEL_WIDTH + (x / TILE_WIDTH)] >= TILES_SOLID_START);
    }
    else
    {
        return false;
    }
}

boolean playerIsAboveASolidTile()
{
    return (isInASolidTile(player.x - PLAYER_COLLISION_RANGE, player.y + 1) ||
            isInASolidTile(player.x + PLAYER_COLLISION_RANGE, player.y + 1));
}

boolean playerIsUnderASolidTile()
{
    return (isInASolidTile(player.x - PLAYER_COLLISION_RANGE, player.y - PLAYER_HEIGHT) ||
            isInASolidTile(player.x + PLAYER_COLLISION_RANGE, player.y - PLAYER_HEIGHT));
}

boolean playerIsAgainstASolidTile()
{
    int16_t offsetX = (player.dx < 0 ? -PLAYER_COLLISION_RANGE - 1 : PLAYER_COLLISION_RANGE + 1);
    return (isInASolidTile(player.x + offsetX, player.y - PLAYER_HEIGHT + 1) ||
            isInASolidTile(player.x + offsetX, player.y));
}

void updateGame()
{
    // In the air and not at terminal velocity?
    if (!playerIsAboveASolidTile() && (player.dy < MAX_FALL_SPEED))
    {
        player.dy += FALL_STRENGTH; // Apply more gravity.
    }

    // Apply the velocity.
    player.y += player.dy;

    // Falling and Hit the ground?
    if ((player.dy > 0) && playerIsAboveASolidTile())
    {
        player.dy = 0;
        player.y = (int)((player.y + 1) / TILE_HEIGHT) * TILE_HEIGHT - 1;

        // re-init the state we're currently in.
        changeState(&player, player.state);
    }

    // Else, Jumping and hitting your head?
    else if ((player.dy < 0) && playerIsUnderASolidTile())
    {
        player.dy = 0;
        player.y = ((int)((player.y - PLAYER_HEIGHT) / TILE_HEIGHT) + 1) * TILE_HEIGHT + PLAYER_HEIGHT - 1;
    }

    // If off the ground...
    if (!playerIsAboveASolidTile())
    {
        // Going up
        if (player.dy < -FLOAT_THRESHOLD)
        {
            player.tick = 0;
            player.frame = 0;
            player.anim = ANIM_JUMP;
        }
        else
        {
            // Going down
            if (player.dy > FLOAT_THRESHOLD)
            {
                player.tick = 0;
                player.frame = 0;
                player.anim = ANIM_FALL;
            }

            // floating
            else
            {
                player.tick = 0;
                player.frame = 0;
                player.anim = ANIM_FLOAT;
            }
        }
    }

    switch (player.state)
    {

    case STATE_MOVING:
        // Move the player, and contrain her to the level width.
        if (!playerIsAgainstASolidTile())
        {
            player.x += player.dx;
        }

        player.x = constrain(player.x, PLAYER_WIDTH / 2, LEVEL_WIDTH * TILE_WIDTH - PLAYER_WIDTH / 2);
        break;

    case STATE_POOPING:
        // If we've got some poops left, and we're a little way through the poop animation, drop one.
        if (treatsInBelly)
        {
            treatsInBelly--;
            char dir = player.dx < 0 ? -1 : 1;
            poop[poops].x = player.x - (PLAYER_X_OFFSET * dir) + (POOP_X_OFFSET * dir);
            poop[poops].y = player.y;
            poops++;
        }
        break;
    }

    // Have we hit any treats? (either by walking into them, or falling on them
    collisionDetection();

    // Are we counting down to a next state?
    if (player.ticksUntilNextState > 0)
    {
        player.ticksUntilNextState--;
        if (player.ticksUntilNextState == 0)
        {
            changeState(&player, player.nextState);
        }
    }

    // Animate the player.
    player.tick++;
    if (player.tick >= anims[player.anim].frameDelay)
    {
        player.tick = 0;
        player.frame++;
        if (player.frame >= anims[player.anim].length)
        {
            player.frame = 0;
        }
    }

    // Animate the flowers.
    flowerTick++;
    if (flowerTick >= FLOWER_TICKS)
    {
        flowerTick = 0;
        flowerFrame++;
        if (flowerFrame >= FLOWER_FRAMES)
        {
            flowerFrame = 0;
        }
    }
}

void render()
{

    // Draws the sprites to a hidden window, and then shifts into view when done.
    // We need to do it this way, otherwise there's a lot of flickering caused by the .cls() and time it takes to blit
    // everything.

    // Clear everything, ready for blitting.
    vfd.cls();

    // Center the display on the player, constrained by the level size.
    int16_t dispX = constrain(player.x - VFD_DISPLAY_WIDTH / 2, 0, LEVEL_WIDTH * TILE_WIDTH - VFD_DISPLAY_WIDTH);
    int16_t dispY = constrain(player.y - PLAYER_Y_DISPLAY_OFFSET, 0, LEVEL_HEIGHT * TILE_HEIGHT - VFD_DISPLAY_HEIGHT);

    // draw the background.
    // We need to know haw much of the first tile is over the edge.
    uint8_t tileXoffset = dispX % TILE_WIDTH;
    uint8_t tileYoffset = dispY % TILE_HEIGHT;

    // We then go through all the tiles on the screen (+1 if the first tile is half over the edge).
    for (uint8_t y = 0; y < VFD_DISPLAY_HEIGHT / TILE_HEIGHT + (tileYoffset > 0 ? 1 : 0); y++)
    {
        for (uint8_t x = 0; x < VFD_DISPLAY_WIDTH / TILE_WIDTH + (tileXoffset > 0 ? 1 : 0); x++)
        {
            // Get the tile from the map.
            uint16_t mapPos = (y + dispY / TILE_HEIGHT) * LEVEL_WIDTH + (x + dispX / TILE_WIDTH);
            uint8_t mapTile = mapData[mapPos];

            if (mapTile == TILE_FLOWER)
            {
                // If it's a flower, animate it (flowerFrame is set in updateGame()).
                mapTile += flowerFrame;
            }

            // If it's not blank, draw the tile.
            if (mapTile)
            {
                vfd.blit(
                    tiles[mapTile - 1],
                    x * TILE_WIDTH - tileXoffset,
                    y * TILE_HEIGHT - tileYoffset,
                    false);
            }
        }
    }

    // Blit the treats
    // There the .blit() function already checks if a sprite's X,Y position is on the screen, so I don't check here.
    for (uint8_t t = 0; t < treats; t++)
    {
        vfd.blit(
            sprites[TREAT_SPRITE],
            treat[t].x - dispX,
            treat[t].y - dispY,
            false);
    }

    // Blit the poops
    for (uint8_t p = 0; p < poops; p++)
    {
        vfd.blit(
            sprites[POOP_SPRITE],
            poop[p].x - dispX,
            poop[p].y - dispY,
            false);
    }

    // Blit the player ("Sunny" the pooch)
    vfd.blit(
        sprites[anims[player.anim].frames[player.frame]],
        player.x - dispX,
        player.y - dispY,
        player.dx < 0);

    /*
    char buffer[17];
    sprintf(buffer, "%d, %d", dispY / TILE_HEIGHT);
    vfd.printXY(0, 0, buffer);
    */

    // Show the changes by shifting the hidden display into view.
    vfd.scroll(VFD_DISPLAY_WIDTH * VFD_DISPLAY_HEIGHT >> 3, 1, 0);
}

void initLevel()
{
    // Reset the player, treats, and poops.
    player.x = 0;
    player.y = 0;
    player.dx = 0;
    player.dy = 0;
    changeState(&player, STATE_STANDING);

    treats = 0;
    treatsInBelly = 0;

    poops = 0;

    // Copy the level data to the map, picking out the player and treats as we encounter them.
    for (uint8_t y = 0; y < LEVEL_HEIGHT; y++)
    {
        for (uint8_t x = 0; x < LEVEL_WIDTH; x++)
        {
            // Calculate which tile to look at.
            uint16_t mapPos = y * LEVEL_WIDTH + x;

            // The data in that position
            uint8_t mapTile = pgm_read_word_near(level + mapPos);

            switch (mapTile)
            {

            case TILE_PLAYER:
                player.x = x * TILE_WIDTH + TILE_WIDTH / 2;
                player.y = y * TILE_HEIGHT + PLAYER_Y_OFFSET;
                mapTile = 0;
                break;

            case TILE_TREAT:
                if (treats < MAX_TREATS)
                {
                    treat[treats].x = x * TILE_WIDTH + TILE_WIDTH / 2;
                    treat[treats].y = y * TILE_HEIGHT + TREAT_Y_OFFSET;
                    treats++;
                }
                mapTile = 0;
                break;
            }
            mapData[mapPos] = mapTile;
        }
    }
}